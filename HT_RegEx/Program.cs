﻿using System;
using System.Net;
using System.Text.RegularExpressions;


namespace HT_RegEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Match match;

            string URLaddress = "https://otus.ru/";
            Uri customURL = new Uri(URLaddress);
            Console.WriteLine("Происходит парсинг...");
            string pattern = @"href\s*=\s*['""]?(?<url>https?:+[^""=;'>\s]+)['"">\s]";

            string html = new WebClient().DownloadString(customURL);

            match = Regex.Match(html, pattern, RegexOptions.IgnoreCase);

            Console.WriteLine("Найденные адреса:");

            while (match.Success)
            {
                Console.WriteLine($"{match.Groups["url"]} at {match.Groups["url"].Index}");
                match = match.NextMatch();
            }

            Console.ReadKey();
        }
    }
}
